//Arrays - used to store multiple related values in a single variable 
// decalred using square brackets  [] - array literals 

//elements - values inside an array 
// index - 0 (off setting)
//element 1 = index 0

// Syntax: let/ const arrayName = [elementA, elementB, elementC]



let grades = [98,94, 89,90];
let computerBrands = ['Acer', 'Asus' , 'Lenovo' , 'Neo', 'Redfox', 'Gateway' ,
 'Toshiba', 'Fujitsu'] ;


console.log(grades [0]);
console.log (computerBrands [3]);
console.log (grades[10]); // will result as undefined 


let myTasks = [
    'bake sass', 
    'drink html',
    'inhale css', 
    'eat javascript',
];

console.log (myTasks);


//re assigning value 

myTasks [0] = 'hello world';
console.log (myTasks); 

//getting the length of an array 
console.log (computerBrands.length) //length = number of items 

// how to get last index 

// let lastIndex = myTasks.length -1;
// console.log (lastIndex);



// Array Methods 

//1. mutator method - changes or mutates the array 
    //push - 
    //SYNTAX - arrayName.push (elementA, elementB)
//2. non-mutator - does not change or modify 
// 3. iteration - loops designed to perform repetitive tasks on arrays /*


// MUTATOR !!!!

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log ('Current array');
console.log (fruits);

// opush - adds an element in the end of the array and return its length 
let fruitsLength = fruits.push ('Mango') ;
console.log ('Mutated array after push()') ;
console.log (fruits);
console.log (fruitsLength) ;

fruits.push ('Guava', 'Avocado') ;
console.log ('Mutated array after push () ');
console.log (fruits) ;

// pop - removes the last element in an array ans return the removed element 

let removedFruit = fruits.pop ();
console.log ('Mutated array after pop () ');
console.log (fruits) ;
console.log (removedFruit);

//shift - removes an element at the beginning of an array and also return 
// the removed element 

let firstFruit = fruits.shift () ;
console.log ('Mutated array after the shift ()');
console.log (fruits);
console.log (firstFruit);

//unshift - adds one or more elements/s at the beginning of an array 

fruits.unshift ('Lime', 'Gomu-Gomu');
console.log ('Mutated array after the unshift ()');
console.log (fruits);

//splice - simultaneously removes element from a specified index and adds
// elements 

//syntax : arrayName.splice (startingIndex, deleteCount, elements to be added)

fruits.splice (1,2, 'Cherry' , 'Grapes');
console.log ('Mutated array after splice ()');
console.log (fruits);

// sort - rearranges the elements in alphanumeric order 

fruits.sort ();
console


//reverses - 

fruits.reverse () ;
console.log ('Mutated array after reverse () ');
console.log (fruits);

// NON MUTATOR !!!!!

let countries = [ 'US', 'PH', 'CAN', 'SG', 'TH','PH', 'FR', 'DE'] ;

//indexOf ()
//returns te first index of the first matching element found in an array 
// if no match is found, it returns -1

let firstIndex = countries.indexOf ('PH'); 
console.log ('Result of indexOf () method:' + firstIndex);

let invalidCountry = countries.indexOf ('BR');
console.log ('Result of indexOf () method:' + invalidCountry);

//lastIndexOf ()
// returns  the last matching element found in an array 
// syntax: arrayName.lastIndexOf(searchValue);
//arrayName.lastIndexOf (searchValue, fromIndex);

let lastIndex = countries.lastIndexOf ('PH');
console.log ('Result of lastindexOf () method:' + lastIndex);

let lastIndexStart = countries.lastIndexOf ('PH' , 2);
console.log ('Result of lastindexOf () method:' + lastIndexStart);

// slice - slices a portion of an array and return a new array 
//syntax - arrayName.slice (startingIndex);


let slicedArrayA = countries.slice (2);
console.log ('Result from slice method A');
console.log (slicedArrayA); 

let slicedArrayB = countries.slice (2, 4);
console.log ('Result from slice method B');
console.log (slicedArrayB); 


let slicedArrayC = countries.slice (-3);
console.log ('Result from slice method C');
console.log (slicedArrayC); 

// toString 

let stringArray = countries.toString ();
console.log ('Result from toString ()');
console.log (stringArray);

// concat - combines two or more arrays and return the combined result 

let tasksArrayA = ['drink HTML' , 'eat javascript'];
let tasksArrayB = ['inhale CSS' , 'breathe sass'];
let tasksArrayC = ['get git', 'be node']; 

let tasks = tasksArrayA.concat (tasksArrayB);
console.log ('Result from concat()');
console.log(tasks);
let allTasks = tasksArrayA.concat (tasksArrayB, tasksArrayC);
console.log ('Result from concat()');
console.log(allTasks);

let combinedTasks = tasksArrayA.concat ('smell express', 'throw react');
console.log ('Result from concat()');
console.log(combinedTasks);

//join - returns an array as string separated by specified separator

let users = ['John' , 'Jane', 'Joe' , 'Robert'];
console.log (users.join());
console.log (users.join ('')) ;
console.log(users.join(' - '));
console.log(users.join(' & '));


// ITERATION METHODS 

// loops designed ti perform repetitive tasks on arrays 

//foreach
//syntax - arrayName.forEach (function(individualElement) {

countries.forEach(function (country) {
    console.log (country);
})

allTasks.forEach (function(task) {
    console.log (task);
})

let filteredTasks = [];

allTasks.forEach (function (task) {
    if (task.length > 10) {
        filteredTasks.push(task);
    }
})

console.log ('Result from forEach()');
console.log (filteredTasks) ;


//map - iterates on each element and returns new array with different 
//values depending on the results of the function's operation 

let numbers = [1,2,3,4,5] ;

let numberMap = numbers.map  (function (number) {
    return number * number;
})

console.log ('Result from map ()');
console.log(numberMap); 

let numberMap2 = [];
numbers.forEach (function (number) {
    let square = number * number ;
    numberMap2.push (square);
})

console.log ('Result from forEach ()');
console.log(numberMap2);


// every 

let allValid = numbers.every(function (number) {
    return (number < 3);
}) 

console.log ('Result from every () ');
console.log (allValid) ;

//some 

let


//chess 

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log (chessBoard) ;
console.log (chessBoard [1] [4]) ;
console.log ('Pawn moved to f2:' + chessBoard [1] [5]);
console.log ('Knight moves to c7')

